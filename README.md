# README #

Maven Repository hosted in Bitbucket

### In your project's pom, add this ###

```
 <repositories>
   <repository>
      <id>shipserv-maven-repo</id>
      <url>https://bitbucket.org/jmaniago/maven-repo/raw/master/repository/</url>
   </repository>
 </repositories>
```